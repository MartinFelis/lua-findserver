--[[
-- Lua-findserver
--
-- Copyright (c) 2013 Martin Felis <martin@fysx.org>
--
-- Published under the MIT license.
--]]

local socket = require ("socket")

host = host or "*"
port = port or 4550
handshake = "MyHandshakeId"

print ("Binding to host '" .. host .. ":" .. port .. " ...")

udp,err = socket.udp()
if not udp then print(err) os.exit() end

assert (udp:setsockname(host,port))
assert (udp:settimeout(0.1))

print ("sockname: " .. tostring(udp:getsockname()))
local e=false
local l=""

while not e do
	message, address, port = udp:receivefrom()
	if message == handshake then
		print ("Handshake received from: " .. address .. ":" .. port)
		print ("Sending reply...")
		udp:sendto (handshake, address, port)
	end
end

udp:close()
