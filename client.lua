local socket = require ("socket")

host = host or "*"
port = port or 4551
handshake = "MyHandshakeId"

print ("Binding to host '" .. host .. ":" .. port .. " ...")

udp,err = socket.udp()
if not udp then print(err) os.exit() end

assert (udp:setsockname(host,port))
assert (udp:setoption('broadcast', true))
assert (udp:settimeout(1.))

print ("Searching for server...")
udp:sendto(handshake, "255.255.255.255", 4550)

local err = false
local server_address = ""
local server_port = ""

while not err do
	message, address, port, err = udp:receivefrom()
	if message == handshake then
		print ("Received Handshake")
		server_address = address
		server_port = port
		break
	end

	if err then
		print ("Client error: " .. err)
	end

	print ("Searching for server...")
	udp:sendto(handshake, "255.255.255.255", 4554)
end

print ("Found Server: " .. server_address .. ":" .. server_port)

udp:close()
